package com.assignments.wordlookup;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolder extends RecyclerView.ViewHolder {
    private TextView tvTranslated;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        tvTranslated = itemView.findViewById(R.id.translated);
    }

    public TextView getTextViewTranslated() {
        return tvTranslated;
    }
}
